#include <stdio.h>

void nextline(int x); 
void row(int y); 
int j=1; 
int r; 

void nextline(int x)
{  
    
    if(x>0)
    {
        row(j);
        printf("\n");
        j++;
        nextline(x-1);
        
    }
}


void row(int y)
{
    if(y>0)
    {
        printf("%d",y);
        row(y-1);
    }
}

int main()
{
    
    printf("Enter the No. of Rows : ");
    scanf("%d",&r);
    nextline(r);
    return 0;
}
